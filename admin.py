# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from ahorro.models import ahorro, movimiento

admin.site.register(ahorro)
admin.site.register(movimiento)
