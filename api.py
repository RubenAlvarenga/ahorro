
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import filters
from rest_framework import generics
from rest_framework import status
from rest_framework import serializers
from rest_framework.response import Response



from ahorro.serializers import AhorroSerializer
from ahorro.models import ahorro


class AhorroApiView(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = AhorroSerializer
    http_method_names = ['get',]
    lookup_field = 'numero'

    def retrieve(self, request, *args, **kwargs):
    	nroahorro = int(kwargs.get('numero'))

    	try:
    		ahorro_object = ahorro.objects.get(numero=nroahorro)
    	except ahorro.DoesNotExist:
            reply = {
                "ErrorCode": "400",
                "ErrorDescription": "no existe el ahorro {0}".format(nroahorro) ,
                "TransactionDetail": "No se puede completar la operacion",
                "SuccessFlag": "False"
            }
            return Response(reply, status=status.HTTP_400_BAD_REQUEST)
    		
        serializer = AhorroSerializer(ahorro_object)
        return Response(serializer.data)
