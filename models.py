# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class ahorro(models.Model):
    from socio.models import socio
    ESTADO_CHOICES = (
        ('A','ACTIVO'),
        ('C','CANCELADO')
    )    
    numero = models.DecimalField(max_digits=15 ,decimal_places=0, verbose_name='Numero de Cuenta', unique=True)
    titular = models.ForeignKey(socio, verbose_name='Titular', on_delete=models.PROTECT)
    apertura = models.DateField( verbose_name='Fecha de Apertura')
    estado = models.CharField(max_length=1, choices=ESTADO_CHOICES, verbose_name='Estado')
    interes = models.DecimalField(max_digits=3 ,decimal_places=2, verbose_name='Tasa de Interes')

    class Meta:
        ordering = ['numero']
        verbose_name = 'Ahorro'
        verbose_name_plural = 'Ahorros'
    def __unicode__(self):
        return '%s %s' % (self.numero, self.titular)


class movimiento(models.Model):
    CONCEPTO_CHOICES = (
        ('D','DEBITO'),
        ('C','CREDITO')
    )    
    ahorro = models.ForeignKey(ahorro, verbose_name='Ahorro', on_delete=models.PROTECT)
    concepto =models.CharField(max_length=1, choices=CONCEPTO_CHOICES, verbose_name='Concepto')
    documento = models.DecimalField(max_digits=13 ,decimal_places=0, verbose_name='Comprobante', unique=True)
    fecha = models.DateTimeField(verbose_name='Fecha de Movimiento', auto_now_add=True)
    importe = models.DecimalField(max_digits=10 ,decimal_places=0, verbose_name='Importe')

    class Meta:
        verbose_name = 'Movimiento de Ahorro'
        ordering = ('fecha', 'id')


    def __unicode__(self):
        return '%s %s %s %s' % (self.fecha.date(), self.ahorro, self.get_concepto_display(), self.importe)