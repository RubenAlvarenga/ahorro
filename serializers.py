
from rest_framework import serializers

from ahorro.models import ahorro


class AhorroSerializer(serializers.ModelSerializer):
    AccountNumber = serializers.IntegerField(source='numero')
    AccountHolder = serializers.CharField(source='titular')

    class Meta:
        model = ahorro
        fields = ('AccountNumber', 'AccountHolder')
    