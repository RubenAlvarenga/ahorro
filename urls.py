#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from .api import AhorroApiView 




router = routers.DefaultRouter()
router.register(r'consulta', AhorroApiView, base_name="consulta")



urlpatterns = [

    url(r'^', include(router.urls)),
 
]